package decorator.decorators;

import decorator.DecoracteurPersonnage;
import decorator.Personnage;

public class Affichable extends DecoracteurPersonnage {
   public Affichable(Personnage p) {
      super(p);
   }

   @Override
   public void action() {
      personnage.action();
      System.out.println("Affiche le personnage à l'écran...");
   }
}
