package decorator;

/**
 * Notre classe abstraite pour les différents personnages
 */
public abstract class Personnage {
   private String nom;
   private int attaque;
   private int defense;

   public String getNom() {
      return nom;
   }

   protected void setNom(String nom) {
      this.nom = nom;
   }

   public int getAttaque() {
      return attaque;
   }

   protected void setAttaque(int attaque) {
      this.attaque = attaque;
   }

   public int getDefense() {
      return defense;
   }

   protected void setDefense(int defense) {
      this.defense = defense;
   }

   public void action() {
      System.out.println("Action de base...");
   }

   @Override
   public String toString() {
      return getNom() + " | ATK : " + getAttaque() + " | DEF : " + getDefense();
   }
}
