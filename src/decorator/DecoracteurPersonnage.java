package decorator;

/**
 * Notre classe abstraite de décorateur de personnages
 */
public abstract class DecoracteurPersonnage /* TODO ...*/ {
   // TODO rajouter l'attribut "personnage" (attention à sa visibilité et son type !)
   ...

   // TODO affecter l'attribut
   protected DecoracteurPersonnage(...) {
      ...
   }

   /**
    * Méthodes par défaut.
    * Les décorateurs doivent surchargés ce qu'ils veulent changer.
    */

   @Override
   public String getNom() {
      return personnage.getNom();
   }

   @Override
   public int getAttaque() {
      return personnage.getAttaque();
   }

   @Override
   public int getDefense() {
      return personnage.getDefense();
   }

   @Override
   public void action() {
      personnage.action();
   }
}
