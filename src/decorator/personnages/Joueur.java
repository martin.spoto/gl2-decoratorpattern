package decorator.personnages;

import decorator.Personnage;

public class Joueur extends Personnage {
   /**
    * crée un joueur basique
    */
   public Joueur(String nom) {
      setNom(nom);
      setAttaque(10);
      setDefense(10);
   }
}
