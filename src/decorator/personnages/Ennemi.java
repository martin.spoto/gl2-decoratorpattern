package decorator.personnages;

import decorator.Personnage;

public class Ennemi extends Personnage {

   /**
    * crée un ennemi basique
    */
   public Ennemi() {
      setNom("Ennemi");
      setAttaque(10);
      setDefense(10);
   }
}
