import decorator.Personnage;
import decorator.decorators.Affichable;
import decorator.decorators.Bouclier;
import decorator.decorators.Deplacable;
import decorator.decorators.Epee;
import decorator.personnages.Ennemi;
import decorator.personnages.Joueur;

public class Main {

   /*
    * Sortie attendue (exemple) :

      Joueur 1 | ATK : 10 | DEF : 10
      Joueur 2 (+épée) (+bouclier) | ATK : 30 | DEF : 30
      Joueur 3 (+épée) (+épée) | ATK : 50 | DEF : 10
      Ennemi (+bouclier) | ATK : 10 | DEF : 30

      Action de base...

      Action de base...
      Affiche le personnage à l'écran...

      Action de base...
      Affiche le personnage à l'écran...
      Déplace le personnage selon les touches du clavier...

    */
   public static void main(String[] args) {

      /*
         TODO, dans l'ordre recommandé :
            - Compléter les décorations des joueurs et ennemis si dessous
            - Compléter la classe DecorateurPersonnage
            - Compléter la classe Epee
            - Créer une classe Bouclier en copiant la classe Epee
            - Compléter les deux dernières décorations ci-dessous
            - Créer une classe Deplaceable
            - Tester...
       */

      Personnage joueurSimple = new Joueur("Joueur 1");
      System.out.println(joueurSimple);

      Personnage joueurEpeeBouclier = new Joueur("Joueur 2");
      // TODO décorer ce joueur d'une épée et d'un bouclier
      ...
      ...
      System.out.println(joueurEpeeBouclier);

      Personnage joueurDeuxEpees = new Joueur("Joueur 3");
      // TODO décorer ce joueur de deux épées
      ...
      ...
      System.out.println(joueurDeuxEpees);

      // TODO créer un ennemi et le décorer d'un bouclier
      Personnage ennemiBouclier  = ...;
      ...
      System.out.println(ennemiBouclier);

      System.out.println();

      joueurDeuxEpees.action();
      System.out.println();

      // TODO décorer ce joueur avec "Affichable"
      ...
      joueurDeuxEpees.action();
      System.out.println();

      // TODO décorer ce joueur avec "Deplacable"
      joueurDeuxEpees.action();
      System.out.println();
   }
}
